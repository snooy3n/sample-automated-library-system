package ProgAssignment4.LibraryExceptions;

@SuppressWarnings("serial")
public class RetrieveUserException extends UserVerificationException {
	public RetrieveUserException() {
		super("Library User Not Found");
	}
}