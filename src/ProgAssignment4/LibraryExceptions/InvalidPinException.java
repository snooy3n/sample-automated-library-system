package ProgAssignment4.LibraryExceptions;

@SuppressWarnings("serial")
public class InvalidPinException extends UserVerificationException{
	public InvalidPinException(){
		super("Invalid Pin");
	}
}
