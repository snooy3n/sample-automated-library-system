package ProgAssignment4.LibraryExceptions;

@SuppressWarnings("serial")
public abstract class UserVerificationException extends Exception{
	public UserVerificationException(String s){
		super(s);
	}
}

