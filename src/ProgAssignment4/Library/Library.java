package ProgAssignment4.Library;

import java.util.ArrayList;

import org.joda.time.DateTime;

import ProgAssignment4.LibraryExceptions.RetrieveUserException;
import ProgAssignment4.LibraryExceptions.UserVerificationException;
import ProgAssignment4.Users.Member;
import ProgAssignment4.Users.User;

public class Library {
	/*
	 * FIELDS
	 */
	// Users
	private ArrayList<Member> libraryMembers = new ArrayList<Member>();
	private ArrayList<User> libraryAdmins = new ArrayList<User>();
	// Book Catalog
	private ArrayList<Book> books = new ArrayList<Book>();
	public final DateTime libDate;

	public Library(DateTime libDate) {
		this.libDate =libDate;
	}

	public User retrieveUser(String id) throws UserVerificationException {
		for (User u : libraryMembers) {
			if (u.getLibraryID().equals(id)) {
				return u;
			}
		}
		for (User u : libraryAdmins) {
			if (u.getLibraryID().equals(id)) {
				return u;
			}
		}
		throw new RetrieveUserException();
	}
	public Book retrieveBook(String bookID) throws Exception {
		for (Book b : books) {
			if (b.getBookID().equals(bookID)) {
				return b;
			}
		}
		throw new Exception("Book not found");
	}
	public boolean verifyUser(String uid, String pin){
		boolean ret = false;
		for(User u : libraryMembers){
			if(u.getLibraryID().equals(uid) && u.getPin().equals(pin)){
				ret = true;
			}
		}
		for(User u : libraryAdmins){
			if(u.getLibraryID().equals(uid) && u.getPin().equals(pin)){
				ret = true;
			}
		}
		return ret;
	}

	/*
	 * Getters and Setters
	 */
	public ArrayList<Member> getLibraryMembers() {
		return libraryMembers;
	}

	public ArrayList<User> getLibraryAdmins() {
		return libraryAdmins;
	}

	public ArrayList<Book> getBooks() {
		return books;
	}
	
	
	
}
