package ProgAssignment4.Library;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Book {
	private String title;
	private String authors;
	private String subject;
	private String publisher;
	private String publicationYear;
	private String bookID;
	private boolean checkedOut;
	private DateTime checkOutDate;

	public Book(String bookID, String title, String authors, String publisher,
			String publicationyear, String subject) {
		setBookID(bookID);
		setTitle(title);
		setAuthors(authors);
		setSubject(subject);
		setPublisher(publisher);
		setPublicationYear(publicationyear);
		setCheckedOut(false);
		setCheckOutDate(null);
	}

	public boolean hasKeyWord(String keyword) {
		boolean ret = false;
		keyword = keyword.toLowerCase();
		if (title.toLowerCase().contains(keyword)
				|| authors.toLowerCase().contains(keyword)
				|| subject.toLowerCase().contains(keyword)
				|| publisher.toLowerCase().contains(keyword)
				|| bookID.toLowerCase().equals(keyword)
				|| publicationYear.toLowerCase().contains(keyword)) {
			ret = true;
		}
		return ret;
	}

	public void print() {
		System.out.println("ID: " + this.getBookID());
		System.out.println("Title: " + this.getTitle());
		System.out.println("Authors: " + this.getAuthors());
		System.out.println("Publisher: " + this.getPublisher());
		System.out.println("Year: " + this.getPublicationYear());
		System.out.println("Subject: " + this.getSubject());
		DateTimeFormatter dtfout = DateTimeFormat.forPattern("yyyy/MM/dd");
		if (this.isCheckedOut()) {
			System.out.println("Checked out on: "
					+ dtfout.print(this.getCheckOutDate()));
		} else {
			System.out.println("This book is available for checkout");
		}

	}

	public void checkInBook() throws Exception {
		if (!this.isCheckedOut()) {
			throw new Exception("Book is already checked in");
		}
		this.setCheckedOut(false);
		this.setCheckOutDate(null);
	}

	public void checkOut(DateTime d) throws Exception {
		if (this.isCheckedOut()) {
			throw new Exception("This book is already checked out");
		} else {
			this.setCheckOutDate(d);
			this.setCheckedOut(true);
		}
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthors() {
		return authors;
	}

	public void setAuthors(String authors) {
		this.authors = authors;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublicationYear() {
		return publicationYear;
	}

	public void setPublicationYear(String publicationYear) {
		this.publicationYear = publicationYear;
	}

	public String getBookID() {
		return bookID;
	}

	public void setBookID(String bookID) {
		this.bookID = bookID;
	}

	public boolean isCheckedOut() {
		return checkedOut;
	}

	public void setCheckedOut(boolean checkedOut) {
		this.checkedOut = checkedOut;
	}

	public DateTime getCheckOutDate() {
		return checkOutDate;
	}

	public void setCheckOutDate(DateTime checkOutDate) {
		this.checkOutDate = checkOutDate;
	}
}
