package ProgAssignment4.Users;

public enum userGroup {
	STUDENTS(10), PROFESSORS(20), LIBRARIANS(0);
	public String typeString() {
		return this.toString().toLowerCase();
	}
	public int getLoanInterval(){
		return this.loanInterval;
	}
	int loanInterval;

	private userGroup(int n) {
		this.loanInterval = n;
	}
}
