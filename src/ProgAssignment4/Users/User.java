package ProgAssignment4.Users;

public abstract class User {
	private String name;
	private String address;
	private String phoneNumber;
	private String libraryID;
	private String pin;

	public userGroup group;

	public User(String name, String address, String phoneNumber,
			String libraryID, String pin) {
		group = null;
		setName(name);
		setAddress(address);
		setPhoneNumber(phoneNumber);
		setLibraryID(libraryID);
		setPin(pin);
	}
	public User(User u){
		this(u.name,u.address, u.phoneNumber, u.libraryID, u.pin);
	}
	public void print() {
		System.out.println(this.getLibraryID());
		//System.out.println(this.getPin());
		System.out.println(this.getName());
		System.out.println(this.getAddress());
		System.out.println(this.getPhoneNumber());
		System.out.println(this.group.toString());
	}
	public userGroup showUserGroup() {
		return this.group;
	};
	
	// GETTERS AND SETTERS
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLibraryID() {
		return libraryID;
	}

	public void setLibraryID(String libraryID) {
		this.libraryID = libraryID;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

}
