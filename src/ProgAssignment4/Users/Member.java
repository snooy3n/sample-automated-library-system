package ProgAssignment4.Users;

import java.util.Map;
import java.util.TreeMap;

import org.joda.time.*;

public class Member extends User {
	private Map<String, DateTime> checkedOutBooks = new TreeMap<String, DateTime>();
	private boolean blocked = false;
	private int overdueBooks = 0;

	public Member(String libraryID, String pin, String name, String address,
			String phoneNumber, userGroup G) {
		super(name, address, phoneNumber, libraryID, pin);
		group = G;
	}

	public Member(Member m) {
		super((User) m);
		this.group = m.group;
	}

	public Map<String, DateTime> getCheckedOutBooks() {
		return checkedOutBooks;
	}

	public void returnBook(String bookID) throws Exception {
		if (!this.checkedOutBooks.containsKey(bookID)) {
			throw new Exception(
					"Cannot find book in member's checked out books");
		}
		this.checkedOutBooks.remove(bookID);
	}

	public void print() {
		super.print();
		if (getOverdueBooks() > 0) {
			System.out.println("User currently has overdue books");
		}
		if (this.isBlocked()) {
			System.out.println("User is currently blocked");
		}

	}

	public int checkOverdueBooks(DateTime libDate) {
		int ret = 0;
		for (DateTime checkOutDate : checkedOutBooks.values()) {
			if ((int) Days.daysBetween(checkOutDate.toLocalDate(),
					libDate.toLocalDate()).getDays() > this.group
					.getLoanInterval()) {
				ret++;
			}
		}
		this.overdueBooks = ret;
		return ret;
	}

	public int checkAndBlock(DateTime libDate) {
		int ret = this.checkOverdueBooks(libDate);
		if (ret > 0) {
			this.setBlocked(true);
		}
		if (ret == 0) {
			this.setBlocked(false);
		}
		return ret;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public int getOverdueBooks() {
		return overdueBooks;
	}

	public int numberCheckedOutBooks() {
		return this.checkedOutBooks.size();
	}
}
