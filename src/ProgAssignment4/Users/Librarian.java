package ProgAssignment4.Users;

public class Librarian extends User {

	public Librarian(String name, String address, String phoneNumber,
			String libraryID, String pin) {
		super(name, address, phoneNumber, libraryID, pin);
		group = userGroup.LIBRARIANS;
	}
	public Librarian(Librarian L){
		super((User)L);
		group = userGroup.LIBRARIANS;
	}

}
