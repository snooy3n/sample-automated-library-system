package ProgAssignment4.LibrarySystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.TreeMap;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import ProgAssignment4.Library.Book;
import ProgAssignment4.Library.Library;
import ProgAssignment4.Users.Librarian;
import ProgAssignment4.Users.Member;
import ProgAssignment4.Users.User;
import ProgAssignment4.Users.userGroup;

//Loads library from file at program initiating
//This class can only be used for a brand new library object and cannot modify existing libraries

public class LibraryFileLoader {
	Library lib;
	File libDataFile;
	Scanner libScanner;

	// Constructing a fileLoader will open a file and create a library reference
	public LibraryFileLoader(String s, Library L) throws FileNotFoundException {
		try {
			openFile(s);
		} catch (Exception e) {
			throw e;
		}
		lib = L;
	}

	// Update Library reads and parses the entire file and adds all information
	// to the library
	public void updateLibrary() {
		new libDataParser(libScanner);
		libScanner.close();
	}

	// I maintain a File reference in case I want to add writing operations
	private void openFile(String s) throws FileNotFoundException {
		try {
			this.libDataFile = new File(s);
			this.libScanner = new Scanner(libDataFile);
		} catch (Exception e) {
			System.err.println(e + "Error Opening File");
			throw e;
		}
	}

	class libDataParser {
		final String labelDelim = "::::::::::";
		Scanner parser;
		ArrayList<Member> allMembers = new ArrayList<Member>();
		ArrayList<User> admins = new ArrayList<User>();
		ArrayList<Book> books = new ArrayList<Book>();

		public libDataParser(Scanner s) {
			parser = s;
			parse();
		}

		private void parse() {
			// String nextLine = parser.nextLine();

			if (parser.nextLine().equals(labelDelim)) {
				parseSection(parser); // books
				lib.getBooks().addAll(this.books);
				/*
				 * for(Book b : lib.getBooks()){
				 * System.out.println("Book : "+b.getTitle
				 * ()+" with ID "+b.getBookID
				 * ()+" is checked out "+b.isCheckedOut()); }
				 */
				parseSection(parser); // students
				parseSection(parser); // professors
				lib.getLibraryMembers().addAll(this.allMembers);
				/*
				 * System.out.println("Members loaded"); for(Book b :
				 * lib.getBooks()){
				 * System.out.println("Book : "+b.getTitle()+" with ID "
				 * +b.getBookID()+" is checked out "+b.isCheckedOut()); }
				 */
				parseSection(parser); // librarians
				lib.getLibraryAdmins().addAll(this.admins);
			}

		}

		private void parseSection(Scanner sectionParser) {
			// Scanner get's passed in while on first line "::::::::::"
			// Next line identifies section type
			String sectionType = sectionParser.nextLine().toLowerCase().trim();
			// System.out.println("SectionType is now " + sectionType);
			sectionParser.nextLine(); // Advance scanner past 2nd section
										// delimiter
			if (sectionType.equals("books")) {
				parseBooks(sectionParser);
			} else if (sectionType.equals("students")
					|| sectionType.equals("professors")) {
				parseMember(sectionType, sectionParser);
			} else if (sectionType.equals("librarians")) {
				parseLibrarians(sectionParser);
			}
		}

		private void parseMember(String sectionType, Scanner sectionParser) {
			// System.out.println("Member parser is now parsing " +
			// sectionType);
			String currentLine = sectionParser.nextLine();
			userGroup group;
			if (sectionType.equals("students")) {
				group = userGroup.STUDENTS;
			} else {
				group = userGroup.PROFESSORS;
			}
			while (sectionParser.hasNextLine()
					&& !currentLine.equals(labelDelim)) {
				String libraryID = currentLine;
				currentLine = sectionParser.nextLine();

				String pin = currentLine;
				currentLine = sectionParser.nextLine();

				String name = currentLine;
				currentLine = sectionParser.nextLine();

				String address = currentLine;
				currentLine = sectionParser.nextLine();

				String phoneNumber = currentLine;
				currentLine = sectionParser.nextLine();

				String checkedOutBooks = currentLine;
				Member member = new Member(libraryID, pin, name, address,
						phoneNumber, group);
				member.getCheckedOutBooks().putAll(
						checkOutBookParser(checkedOutBooks));
				currentLine = sectionParser.nextLine();

				// System.out.println("Checking for blocked line. Line is "+currentLine);
				if (currentLine.equals("[Blocked]")) {
					// System.out.println("Now blocking "+member.getName());
					member.setBlocked(true);
					currentLine = sectionParser.nextLine();
				}
				member.checkOverdueBooks(lib.libDate);
				if(member.getOverdueBooks() > 0){
					member.setBlocked(true);
				}

				allMembers.add(member);

			}
			// System.out.println("Finished parsing "+ parseCount+" "+
			// sectionType);
		}

		Map<String, DateTime> checkOutBookParser(String cb) {
			// System.out.println("START COBPARSER with input string " + cb);
			Map<String, DateTime> ret = new TreeMap<String, DateTime>();
			Scanner input = new Scanner(cb);
			Scanner tokenScanner;
			input.useDelimiter("[,]");
			String token;
			String dateToken, bookIDToken;
			while (input.hasNext()) {
				token = input.next();
				// System.out.println("token: " + token);
				tokenScanner = new Scanner(token);
				tokenScanner.useDelimiter("[:]");
				dateToken = tokenScanner.next();
				bookIDToken = tokenScanner.next().trim().toLowerCase();
				tokenScanner.close();
				DateTimeFormatter formatter = DateTimeFormat
						.forPattern("yyyy/MM/dd");
				DateTime dt = formatter.parseDateTime(dateToken);
				try {
					ret.put(lib.retrieveBook(bookIDToken).getBookID(), dt);
					lib.retrieveBook(bookIDToken).checkOut(dt);
					lib.retrieveBook(bookIDToken).setCheckedOut(true);
				} catch (Exception e) {
					System.err.println("Parser Error: could not retrieve book "
							+ bookIDToken);
					e.printStackTrace();
				}
			}
			// System.out.println("END COBPARSER");
			input.close();
			return ret;
		}

		// Individual parsers for different library data types
		// Each parser is passed the scanner, which begins at the line
		// immediately following the label for its respective section.
		// Parser functions terminate when scanner reached EOF or another Label
		// delimiter
		private void parseBooks(Scanner bookParser) {
			// System.out.println("Now Parsing Books");
			String currentLine = bookParser.nextLine(); // currentLine
														// initialized to first
														// bookID. Scanner is at
														// the beginning of 2nd

			while (bookParser.hasNextLine() && !currentLine.equals(labelDelim)) {
				// System.out.println("curentLine = " + currentLine);

				String bookID = currentLine.toLowerCase();
				String title = bookParser.nextLine();
				
				String authors = bookParser.nextLine();
		
				String publisher = bookParser.nextLine();
		
				String year = bookParser.nextLine();
			
				String subject = bookParser.nextLine();
			

				books.add(new Book(bookID, title, authors, publisher, year,
						subject));

				currentLine = bookParser.nextLine();

			}
			// System.out.println("Finished parsing " + books.size() +
			// " books.");
			/*
			 * for (Book b : books) { b.print(); }
			 */
			// System.out.println("Currentline is "+currentLine+" ");
			// ***Verified that this function returns wh

		}

		private void parseLibrarians(Scanner sectionParser) {
			// System.out.println("Now Loading Librarians");
			String currentLine = sectionParser.nextLine();
			while (sectionParser.hasNextLine()
					&& !currentLine.equals(labelDelim)) {
				String libraryID = currentLine;
				currentLine = sectionParser.nextLine();

				String pin = currentLine;
				currentLine = sectionParser.nextLine();

				String name = currentLine;
				currentLine = sectionParser.nextLine();

				String address = currentLine;
				currentLine = sectionParser.nextLine();

				String phoneNumber = currentLine;
				if (sectionParser.hasNextLine()) {
					currentLine = sectionParser.nextLine();
				}
				admins.add(new Librarian(name, address, phoneNumber, libraryID,
						pin));
			}
		}

	}
}
