/* Sean Nguyen 3207719 CS56 Fall 2014 
 * This class contains the main function of the LibrarySystem
 */

package ProgAssignment4.LibrarySystem;

import java.util.Scanner;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class LibrarySystem {
	
	public static void main(String[] args){
		String libPath;
		Scanner input = new Scanner(System.in);
		System.out.println("Welcome to the Automated Library System");
		System.out.println("Enter Library Database Path");
		libPath = input.nextLine();
		new Session(libPath, getLibDateTime(input));
		System.exit(0);
	}
	static DateTime getLibDateTime(Scanner s){
		String date;
		DateTime ret;
		System.out.print("Enter Date (Format: yyyy/MM/dd): ");
		date = s.nextLine().trim();
		//date = "2014/12/02";
		System.out.println("");
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy/MM/dd");
		int i = 0;
		int maxTries = 3;
		while(true){
			try{
				ret = formatter.parseDateTime(date);
				break;
			}catch(IllegalArgumentException e){
				System.err.println("Invalid date format! Please try again.");
				if(++i == maxTries){
					System.err.println("Invalid date format! Please restart program");
					System.exit(1);
				}
			}
		}
		return ret;
	}
}
