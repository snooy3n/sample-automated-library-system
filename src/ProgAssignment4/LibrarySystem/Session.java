package ProgAssignment4.LibrarySystem;

import org.joda.time.DateTime;

import ProgAssignment4.Library.Library;
import ProgAssignment4.Users.User;

public class Session {
	DateTime sessionDate;
	private User sessionUser;
	private Library sessionLib;
	private sessionState state = sessionState.LOGGEDOUT;
	public Session(String libraryPath, DateTime date){
		
		setSessionLib(new Library(date));
		sessionDate = date;
		LibraryFileLoader libraryLoader;
		try{
			System.out.println("Loading "+libraryPath);
			libraryLoader = new LibraryFileLoader(libraryPath, getSessionLib());
			libraryLoader.updateLibrary();
			/*System.out.println("Library loaded with:");
			System.out.println(sessionLib.getBooks().size()+" books");
			System.out.println(sessionLib.getLibraryMembers().size()+" members");
			System.out.println(sessionLib.getLibraryAdmins().size()+" librarians");*/
			
		}
		catch(Exception e){
			System.err.println(e);
			System.exit(1);
		}
		while(getState()!=sessionState.TERMINATE){
			new InterfaceBuilder(this);
		}
	}
	public sessionState getState() {
		return state;
	}
	public void setState(sessionState state) {
		this.state = state;
	}
	public Library getSessionLib() {
		return sessionLib;
	}
	public void setSessionLib(Library sessionLib) {
		this.sessionLib = sessionLib;
	}
	public User getSessionUser() {
		return sessionUser;
	}
	public void setSessionUser(User sessionUser) {
		this.sessionUser = sessionUser;
	}
}
