package ProgAssignment4.LibrarySystem;

import ProgAssignment4.ProgramInterface.ConsoleInterfaceFactory;
import ProgAssignment4.ProgramInterface.sessionInterface;

public class InterfaceBuilder {
	Session interfaceSession;
	sessionInterface chosenInterface;
	public InterfaceBuilder(Session s){
		interfaceSession = s;
		ConsoleInterfaceFactory factory = new ConsoleInterfaceFactory();
		chosenInterface = factory.outputInterface(s); 
		this.chosenInterface.run();
	}

}
