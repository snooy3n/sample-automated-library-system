package ProgAssignment4.ProgramInterface;

import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.LibrarySystem.sessionState;
import ProgAssignment4.Users.userGroup;

public class ConsoleInterfaceFactory {
	public ConsoleInterfaceFactory(){
	}
	public sessionInterface outputInterface(Session S){
		if(S.getState() == sessionState.LOGGEDOUT){
			return new ConsoleLoginInterface(S);
		}
		if(S.getState() == sessionState.LOGGEDIN){
			if(S.getSessionUser().group == userGroup.LIBRARIANS){
				return new ConsoleLibrarianInterface(S);
			}
			else{
				return new ConsoleMemberInterface(S);
			}
		}
		else{
			S.setState(sessionState.TERMINATE);
			return null;
		}
		
	}
}

