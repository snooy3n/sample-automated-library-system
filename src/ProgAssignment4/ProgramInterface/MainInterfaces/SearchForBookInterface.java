package ProgAssignment4.ProgramInterface.MainInterfaces;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

import ProgAssignment4.Library.Book;
import ProgAssignment4.LibrarySystem.Session;

public class SearchForBookInterface extends MainInterface {

	public SearchForBookInterface(Session s) {
		super(s);
	}

	@Override
	public void run() {
		String query;
		Map<Integer, Book> availableBooks = new TreeMap<Integer, Book>();
		ArrayList<Book> unavailableBooks = new ArrayList<Book>();
		Scanner input = new Scanner(System.in);
		// Search
		boolean again = true;
		while (again) {
			availableBooks.clear();
			unavailableBooks.clear();
			System.out.println("Enter Search Keyword: ");
			try {
				int i = 1;
				query = input.nextLine();
				for (Book b : session.getSessionLib().getBooks()) {
					if (b.hasKeyWord(query)) {
						if (b.isCheckedOut()) {
							unavailableBooks.add(b);
						} else {
							availableBooks.put(i, b);
							i++;
						}
					}
				}
				if (i == 0) {
					System.out.println("No results found");
					again = searchAgain(input);
					if (!again) {
						return;
					}
					else{
						continue;
					}
				} else {
					System.out.println("********************************");
					System.out.println("Books available for checkout:");
					System.out.println("********************************");
					for (Entry<Integer, Book> be : availableBooks.entrySet()) {
						System.out.println("(" + be.getKey() + ")"
								+ "-------------");
						be.getValue().print();
					}
					System.out.println("********************************");
					System.out.println("Books unavailable for checkout: ");
					System.out.println("********************************");
					int j = 0;
					for (Book ube : unavailableBooks) {
						j++;
						System.out.println("(" + j + ")"
								+ "-------------");
						ube.print();
					}
				}

			} catch (Exception e) {
				System.err.println("Search failed please try again");
			}
			again = searchAgain(input);
		}
		// new SearchForBook(session.getSessionLib(), input.nextLine()).run();
	}

	public boolean searchAgain(Scanner input) {
		System.out.println("Search again? (Y/N)");
		while (true) {
			String s = input.nextLine();
			if (s.toLowerCase().trim().equals("y")) {
				return true;
			} else if (s.toLowerCase().trim().equals("n")) {
				return false;
			} else {
				System.err.println("Invalid Input");
				continue;
			}
		}
	}

	@Override
	public String toString() {
		return "Search For a Book";
	}

}
