package ProgAssignment4.ProgramInterface.MainInterfaces;

import java.util.Scanner;

import ProgAssignment4.Library.Library;
import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.Users.Member;

public class ShowOverdueUsersInterface extends MainInterface {
	Scanner input;
	public ShowOverdueUsersInterface(Session s) {
		super(s);
	}

	@Override
	public void run() {
		Library l = session.getSessionLib();
		for(Member m : l.getLibraryMembers()){
			if(m.checkOverdueBooks(l.libDate) > 0){
				m.print();
			}
		}
	}

	@Override
	public String toString() {
		return "Show Users with Overdue Books";
	}

}
