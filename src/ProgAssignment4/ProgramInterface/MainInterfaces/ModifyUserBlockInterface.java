package ProgAssignment4.ProgramInterface.MainInterfaces;

import java.util.Scanner;

import ProgAssignment4.LibraryExceptions.UserVerificationException;
import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.Users.Member;

public class ModifyUserBlockInterface extends MainInterface implements Runnable {
	Scanner input;
	public ModifyUserBlockInterface(Session s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		input = new Scanner(System.in);
		try {
			System.out.println("Enter Target Library Member ID: ");
			String uid = input.nextLine();
			Member m = (Member) session.getSessionLib().retrieveUser(uid);
			int choice = 0;
			while(choice == 0){
				System.out.println("(1) - Block User: "+m.getName());
				System.out.println("(2) - Unblock User: "+m.getName());
				choice = input.nextInt();
				if(choice == 1){
					m.setBlocked(true);
					System.out.println(m.getName()+" - "+m.getLibraryID()+" is now [Blocked]");
					return;
				}
				else if (choice == 2){
					m.setBlocked(false);
					System.out.println(m.getName()+" - "+m.getLibraryID()+" is now [Unblocked]");
				}
				else{
					System.err.println("Invalid Input");
				}
			}
		} catch (UserVerificationException e) {
			System.err.println(e.getMessage());
		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Block or Unblock A User";
	}

}
