package ProgAssignment4.ProgramInterface.MainInterfaces;

import java.util.Scanner;

import ProgAssignment4.Library.Book;
import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.Users.Member;

public class BookCheckOutInterface extends MainInterface implements Runnable {

	public BookCheckOutInterface(Session s) {
		super(s);
	}
	
	@Override
	public void run() {
		Member m = (Member)session.getSessionUser();
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		if(m.isBlocked()){
			System.out.println("Your account is blocked.");
			System.out.println("Please return overdue books or see a librarian for assistance");
			System.out.println("Book Checkout Aborted");
			return;
		}
		else{
			while(true){
				System.out.println("Enter bookID of book to be checked out");
				Book b;
				String bookID = input.nextLine().toLowerCase();
				try{
				b = session.getSessionLib().retrieveBook(bookID);
				b.checkOut(session.getSessionLib().libDate);
				}catch(Exception e){
					System.err.println(e.getMessage());
					System.err.println("Error checking out book please try again see librarian for assistance");
					return;
				}
				m.getCheckedOutBooks().put(b.getBookID(), session.getSessionLib().libDate);
				System.out.println(b.getTitle()+" has been successfully loaned to "+m.group.toString()+" "+m.getName());
				System.out.println("Check out another book? (Y/N)");
				while (true) {
					String choice = input.nextLine().trim().toLowerCase();
					if (choice.equals("y")) {
						break;
					}
					if (choice.equals("n")) {
						System.out.println("Book checkout complete");
						if(m.checkOverdueBooks(session.getSessionLib().libDate)>0){
							m.setBlocked(true);
							System.out.println("You have overdue books and are now blocked from checking out any more books.");
							System.out.println("Please return overdue books or see a librarian for assistance.");
						}
						return;
						
					} else {
						System.out.println("Please Enter (Y) / (N)");
					}
				}
			}
		}
	}

	@Override
	public String toString() {
		return "Check Out A Book";
	}
}
