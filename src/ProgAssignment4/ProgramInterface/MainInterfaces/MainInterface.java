package ProgAssignment4.ProgramInterface.MainInterfaces;

import ProgAssignment4.LibrarySystem.Session;

public abstract class MainInterface implements Runnable {
	Session session;

	public MainInterface(Session s) {
		session = s;
	}

	@Override
	abstract public void run();

	public abstract String toString();
}
