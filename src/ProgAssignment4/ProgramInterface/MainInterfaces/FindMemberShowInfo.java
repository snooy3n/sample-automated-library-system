package ProgAssignment4.ProgramInterface.MainInterfaces;

import java.util.Scanner;
import java.util.Map.Entry;

import org.joda.time.DateTime;

import ProgAssignment4.Library.Library;
import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.Users.Member;

public class FindMemberShowInfo extends MainInterface {



	private Scanner input;

	public FindMemberShowInfo(Session s) {
		super(s);
	}

	@Override
	public String toString() {
		return "View Library Member Info"; 
	}

	@Override
	public void run() {
		Library tmp = session.getSessionLib();
		input = new Scanner(System.in);
		System.out.println("Enter User Library ID: ");
		try{
			String uid = input.nextLine();
			Member m =(Member) tmp.retrieveUser(uid);
			m.print();
			if(!m.getCheckedOutBooks().isEmpty()){
				System.out.println(m.getName()+" has checked out the following books: ");
				for(Entry<String, DateTime> e : m.getCheckedOutBooks().entrySet()){
					System.out.println(" ~ bookID "+e.getKey()+" on "+e.getValue().toDate());
				}
			}
			else{
				System.out.println("User has not checked out any books.");
			}
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}

}
