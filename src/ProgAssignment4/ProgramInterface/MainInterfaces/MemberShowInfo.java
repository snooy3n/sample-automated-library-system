package ProgAssignment4.ProgramInterface.MainInterfaces;

import java.util.Map.Entry;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import ProgAssignment4.Users.*;
import ProgAssignment4.LibrarySystem.Session;

public class MemberShowInfo extends MainInterface {

	public MemberShowInfo(Session s) {
		super(s);
	}

	@Override
	public void run() {
		Member m = (Member)session.getSessionUser();
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy/MM/dd");
		System.out.println();
		System.out.println("------------------------");
		System.out.println("Your Info");
		System.out.println("------------------------");
		m.print();
		if(!m.getCheckedOutBooks().isEmpty()){
			System.out.println(m.getName()+" has checked out the following books: ");
			for(Entry<String, DateTime> e : m.getCheckedOutBooks().entrySet()){
				System.out.println(" ~ bookID "+e.getKey()+" on "+dtf.print(e.getValue()));
			}
		}
		else{
			System.out.println("User has not checked out any books.");
		}
		System.out.println("------------------------");
		System.out.println("");
	}

	@Override
	public String toString() {
		return "Show User Information";
	}
	

}
