package ProgAssignment4.ProgramInterface.MainInterfaces;

import java.util.InputMismatchException;
import java.util.Scanner;

import ProgAssignment4.LibrarySystem.Session;

public class TerminateInterface extends MainInterface {

	public TerminateInterface(Session s) {
		super(s);
	}

	@Override
	public void run() {
		System.out.println("Would you like to terminate the program? (Y/N)");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		try{
			String ans = input.nextLine().toLowerCase();
			if(ans.equals("y")){
				System.out.println("Library System Terminated");
				System.exit(0);
			}
			else if(ans.equals("n")){
				return;
			}
		}catch(InputMismatchException e){
			System.out.println(e.getMessage());
		}
	}

	@Override
	public String toString() {
		return "Exit Program";
	}

}
