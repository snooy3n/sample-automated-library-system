package ProgAssignment4.ProgramInterface.MainInterfaces;

import java.util.InputMismatchException;
import java.util.Scanner;

import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.LibrarySystem.sessionState;

public class LogOutInterface extends MainInterface {
	
	
	public LogOutInterface(Session s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		System.out.println("Are you sure you want to log out? (Y/N)");
		try{
			String ans = input.nextLine().toLowerCase();
			if(ans.equals("y")){
				session.setState(sessionState.LOGGEDOUT);
			}
			else if(ans.equals("n")){
				return;
			}
		}catch(InputMismatchException e){
			System.out.println(e.getMessage());
		}
	}

	@Override
	public String toString() {
		return "Log Out";
	}

}
