package ProgAssignment4.ProgramInterface.MainInterfaces;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

import ProgAssignment4.Library.Book;
import ProgAssignment4.Library.Library;
import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.Users.Member;

public class BookReturnInterface extends MainInterface {

	public BookReturnInterface(Session s) {
		super(s);
	}

	@Override
	public void run() {
		Library l = session.getSessionLib();
		if(((Member) session.getSessionUser()).numberCheckedOutBooks()==0){
			System.out.println("No Books to Return");
			return;
		}
		Map<Integer,String> booksToReturn = new HashMap<Integer, String>();
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int i = 1;
		System.out.println("Choose book to return:");
		
		for(String bookID : ((Member) session.getSessionUser()).getCheckedOutBooks().keySet()){
			booksToReturn.put(i, bookID);
			try {
				System.out.println("("+i+")"+" - "+l.retrieveBook(bookID).getTitle()+" ~ "+l.retrieveBook(bookID).getBookID());
			} catch (Exception e) {
				e.printStackTrace();
			}
			i++;
		}
		System.out.println("(0) - Cancel Return");
		while(true){
			try{
				int input2 = input.nextInt();
				if(input2 == 0){
					System.out.println("Aborting Book Return");
					return;
				}
				if(input2 >= i){
					throw new InputMismatchException("Invalid Input: Please Try Again or Enter 0 to Abort");
				}
				//new BookReturn(l, session.getSessionUser().getLibraryID(), booksToReturn.get(input2)).run();
				Book ret = l.retrieveBook(booksToReturn.get(input2));
				ret.checkInBook();
				((Member) session.getSessionUser()).returnBook(ret.getBookID());
				((Member) session.getSessionUser()).checkAndBlock(l.libDate);
				break;
			}catch(InputMismatchException IE){
				continue;
			} catch (Exception e) {
				System.out.println(e.getMessage());
				continue;
			}
		}
		
		
	}

	@Override
	public String toString() {
		return "Return a book";
	}

}
