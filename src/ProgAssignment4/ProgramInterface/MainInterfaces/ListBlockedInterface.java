package ProgAssignment4.ProgramInterface.MainInterfaces;

import ProgAssignment4.Library.Library;
import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.Users.Member;

public class ListBlockedInterface extends MainInterface implements Runnable {

	public ListBlockedInterface(Session s) {
		super(s);
	}

	@Override
	public void run() {
		Library tmp = session.getSessionLib();
		System.out.println("---------------------------");
		System.out.println("Blocked Library Members");
		for(Member m : tmp.getLibraryMembers()){
			if(m.isBlocked()){
				System.out.println("---------------------------");
				m.print();
				System.out.println("---------------------------");
			}
		}
		System.out.println();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Show Blocked Users";
	}

}
