package ProgAssignment4.ProgramInterface;

import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.ProgramInterface.MainInterfaces.FindMemberShowInfo;
import ProgAssignment4.ProgramInterface.MainInterfaces.ListBlockedInterface;
import ProgAssignment4.ProgramInterface.MainInterfaces.ModifyUserBlockInterface;
import ProgAssignment4.ProgramInterface.MainInterfaces.SearchForBookInterface;
import ProgAssignment4.ProgramInterface.MainInterfaces.ShowOverdueUsersInterface;

public class ConsoleLibrarianInterface extends ConsoleUserInterface implements sessionInterface {
	public ConsoleLibrarianInterface(Session s) {
		super(s);
		consoleInterface.put("1", new FindMemberShowInfo(currentSession));
		consoleInterface.put("2", new SearchForBookInterface(currentSession));
		consoleInterface.put("3", new ModifyUserBlockInterface(currentSession));
		consoleInterface.put("4", new ListBlockedInterface(currentSession));
		consoleInterface.put("5", new ShowOverdueUsersInterface(currentSession));
	}

}
