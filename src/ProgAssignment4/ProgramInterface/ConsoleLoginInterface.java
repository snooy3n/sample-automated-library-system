package ProgAssignment4.ProgramInterface;

import java.util.Scanner;

import ProgAssignment4.LibraryExceptions.UserVerificationException;
import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.LibrarySystem.sessionState;

public class ConsoleLoginInterface implements sessionInterface {
	Session currentSession;
	private Scanner consoleInput;

	public ConsoleLoginInterface(Session s) {
		currentSession = s;
	}

	@Override
	public void run() {
		sessionState ret = sessionState.LOGGEDOUT;
		String inputID;
		String inputPIN;
		consoleInput = new Scanner(System.in);
		boolean isVerified = false;
		System.out.print("Enter Your Login ID: ");
		inputID = consoleInput.nextLine();
		System.out.print("Enter Your Login PIN: ");
		inputPIN = consoleInput.nextLine();
		isVerified = currentSession.getSessionLib().verifyUser(inputID,
				inputPIN);
		while (!isVerified) {
			System.out.println("Wrong password");
			System.out.println("Would you like to try again? (Y)/(N)");
			while (true) {
				String choice = consoleInput.nextLine().trim().toLowerCase();
				if (choice.equals("y")) {
					System.out.print("Enter Your Login ID: ");
					inputID = consoleInput.nextLine();
					System.out.print("Enter Your Login PIN: ");
					inputPIN = consoleInput.nextLine();
					isVerified = currentSession.getSessionLib().verifyUser(
							inputID, inputPIN);
					break;
				}
				if (choice.equals("n")) {
					System.out.println("Now Exiting");
					System.exit(0);
				} else {
					System.out.println("Please Enter (Y) / (N)");
				}
			}
		}
		if (isVerified) {
			// If credentials are verified, try to retrieve user from library.
			// If not, log out.
			try {
				currentSession.setSessionUser(currentSession.getSessionLib()
						.retrieveUser(inputID));
				System.out.println();
				System.out.println("-----------------------------------------");
				System.out.println("Welcome "
						+ currentSession.getSessionUser().getName() + "!");
				System.out.println("-----------------------------------------");
			} catch (UserVerificationException e) {
				System.out.println(e.getMessage()
						+ " User database may be corrupt");
				ret = sessionState.LOGGEDOUT;
				consoleInput.close();

			}
			ret = sessionState.LOGGEDIN;
		}

		currentSession.setState(ret);
	}

}
