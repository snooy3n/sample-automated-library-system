package ProgAssignment4.ProgramInterface;

import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.TreeMap;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.ProgramInterface.MainInterfaces.LogOutInterface;
import ProgAssignment4.ProgramInterface.MainInterfaces.TerminateInterface;

public class ConsoleUserInterface implements sessionInterface {
	Session currentSession;
	Map<String, Runnable> consoleInterface = new TreeMap<String, Runnable>();
	public ConsoleUserInterface(Session s){
		currentSession = s;
		consoleInterface.put("z", new LogOutInterface(currentSession));
		consoleInterface.put("x", new TerminateInterface(currentSession));
	}
	@Override
	public void run() {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		String choice;
		System.out.println();
		System.out.println(" -----------------------------");
		System.out.println("|  Choose Library Operation   |");
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy/MM/dd");
		System.out.println("|        "+dtf.print(currentSession.getSessionLib().libDate)+"           |");
		System.out.println(" -----------------------------");
		for (Iterator<String> iterator = consoleInterface.keySet().iterator(); iterator
				.hasNext();) {
			String s = iterator.next();
			System.out.println("("+s+")"+" - "+consoleInterface.get(s).toString());
		}
		while(true){
			try{
				choice = input.nextLine().toLowerCase();
				if (!consoleInterface.containsKey(choice)) {
					throw new InputMismatchException("Invalid Input Please Try Again");}
				consoleInterface.get(choice).run();
				break;
			}catch(InputMismatchException IE){
				System.err.println(IE.getMessage());
				continue;
			}catch(NoSuchElementException e){
				System.err.println("Invalid Input Please Try Again!");
				continue;
			}
		}
		
	}
}
