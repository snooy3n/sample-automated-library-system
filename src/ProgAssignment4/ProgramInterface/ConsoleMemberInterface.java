package ProgAssignment4.ProgramInterface;

import ProgAssignment4.LibrarySystem.Session;
import ProgAssignment4.ProgramInterface.MainInterfaces.BookCheckOutInterface;
import ProgAssignment4.ProgramInterface.MainInterfaces.BookReturnInterface;
import ProgAssignment4.ProgramInterface.MainInterfaces.MemberShowInfo;
import ProgAssignment4.ProgramInterface.MainInterfaces.SearchForBookInterface;

public class ConsoleMemberInterface extends ConsoleUserInterface {

	public ConsoleMemberInterface(Session s) {
		super(s);

		consoleInterface.put("1", new MemberShowInfo(currentSession));
		consoleInterface.put("2", new SearchForBookInterface(currentSession));
		consoleInterface.put("3", new BookCheckOutInterface(currentSession));
		consoleInterface.put("4", new BookReturnInterface(currentSession));
	}
	
}
